const fs = require("fs")
const path = require("path")
const glob = require("glob")
const dir = process.argv[2]
const dirPath = path.join(__dirname, dir)
const slug = require(`slug`)
if (!fs.existsSync(dirPath)) {
  throw new Error(`dirpath doesn't exists: ${dirPath}`)
}

const slidesFilePath = path.join(dirPath, "slides.json")
let existingSlides = {}
if (fs.existsSync(slidesFilePath)) {
  existingSlides = JSON.parse(fs.readFileSync(slidesFilePath, "utf8"))
}

const files = glob.sync(`${dirPath}/*.jpg`)

const allSlides = files
  .map(filePath => filePath.match(/\/([^\/]*)$/)[1])
  .filter(fileName => {
    return !existingSlides.find(slide => {
      return slide.image === fileName
    })
  })
  .reduce((allSlides, fileName) => {
    allSlides.push({
      id: slug(fileName.match(/(.*).jpg$/)[1]),
      image: fileName,
    })
    return allSlides
  }, existingSlides)

fs.writeFileSync(
  // path.join(dirPath, "test.json"),
  slidesFilePath,
  JSON.stringify(allSlides, null, 2),
)
