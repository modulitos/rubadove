import React from "react"
import PropTypes from "prop-types"
import Helmet from "react-helmet"
import { css } from "react-emotion"
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider"
import getMuiTheme from "material-ui/styles/getMuiTheme"

import "./index.css"

const TemplateWrapper = ({ children }) => (
  <MuiThemeProvider theme={getMuiTheme()}>
    <div
      className={css({
        height: "100vh",
        width: "100%",
        boxSizing: "border-box",
        padding: 0,
        margin: 0,
      })}
    >
      <Helmet
        title="Rubadove"
        meta={[
          { name: "description", content: "let's share what we know!" },
          { name: "keywords", content: "rub, dove, rubadove" },
        ]}
      />
      {children()}
    </div>
  </MuiThemeProvider>
)

TemplateWrapper.propTypes = {
  children: PropTypes.func,
}

export default TemplateWrapper
