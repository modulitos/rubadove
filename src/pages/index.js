import React from "react"
import { css } from "react-emotion"
import Stats from "stats.js"
import * as THREE from "three"

const N_OBJECTS = 1500

class IndexPage extends React.Component {
  constructor(props) {
    super(props)
    this.container

    this.camera
    this.scene

    this.meshes = []

    this.clock = new THREE.Clock()

    this.mouseX = 0
    this.mouseY = 0
  }

  componentDidMount() {
    let SCREEN_WIDTH = window.innerWidth
    let SCREEN_HEIGHT = window.innerHeight

    this.windowHalfX = SCREEN_WIDTH / 2
    this.windowHalfY = SCREEN_HEIGHT / 2

    this.camera = new THREE.PerspectiveCamera(
      55,
      window.innerWidth / window.innerHeight,
      1,
      20000,
    )
    this.camera.position.z = 3200

    this.scene = new THREE.Scene()
    this.scene.add(this.camera)

    let light
    light = new THREE.PointLight(0x00ff00, 1, 5500)
    light.position.set(4000, 0, 0)
    this.scene.add(light)

    light = new THREE.PointLight(0xff1100, 1, 5500)
    light.position.set(-4000, 0, 0)
    this.scene.add(light)

    light = new THREE.PointLight(0xffaa00, 2, 3000)
    light.position.set(0, 0, 0)
    this.scene.add(light)

    const material = new THREE.MeshPhongMaterial({
      specular: 0x333333,
      shininess: 100,
    })
    material.wrapAround = true
    material.wrapRGB.set(0.5, 0.5, 0.5)
    material.side = THREE.DoubleSide

    const geometry = new THREE.TorusGeometry(0.5, 0.3, 32, 16, Math.PI * 1.15)

    const group = new THREE.Object3D()

    const dd = 8000

    for (let i = 0; i < N_OBJECTS; i++) {
      const mesh = new THREE.Mesh(geometry, material)

      mesh.position.x = THREE.Math.randFloatSpread(dd)
      mesh.position.y = THREE.Math.randFloatSpread(dd)
      mesh.position.z = THREE.Math.randFloatSpread(dd)

      mesh.rotation.x = Math.random() * 360 * (Math.PI / 180)
      mesh.rotation.y = Math.random() * 360 * (Math.PI / 180)
      mesh.scale.x = mesh.scale.y = mesh.scale.z = Math.random() * 150 + 100

      group.add(mesh)

      this.meshes[i] = mesh
    }

    group.position.y = 5000
    this.scene.add(group)

    // RENDERER
    this.renderer = new THREE.WebGLRenderer({
      clearColor: 0x050505,
      clearAlpha: 1,
      antialias: true,
      alpha: false,
    })
    this.renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT)
    this.renderer.sortObjects = false

    this.renderer.gammaInput = true
    this.renderer.gammaOutput = true
    this.renderer.physicallyBasedShading = true

    this.container.appendChild(this.renderer.domElement)

    // STATS
    this.stats = new Stats()
    this.container.appendChild(this.stats.dom)

    // EVENTS
    const onDocumentMouseMove = event => {
      this.mouseX = (event.clientX - this.windowHalfX) * 10
      this.mouseY = (event.clientY - this.windowHalfY) * 10
    }

    const onWindowResize = event => {
      SCREEN_WIDTH = window.innerWidth
      SCREEN_HEIGHT = window.innerHeight

      this.renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT)

      this.camera.aspect = SCREEN_WIDTH / SCREEN_HEIGHT
      this.camera.updateProjectionMatrix()

      this.windowHalfX = SCREEN_WIDTH / 2
      this.windowHalfY = SCREEN_HEIGHT / 2
    }

    document.addEventListener("mousemove", onDocumentMouseMove, false)
    window.addEventListener("resize", onWindowResize, false)

    const animate = () => {
      requestAnimationFrame(animate)

      this.stats.begin()
      this.renderWebgl()
      this.stats.end()
    }
    animate()
  }

  renderWebgl() {
    this.camera.position.x +=
      (0.125 * this.mouseX - this.camera.position.x) * 0.0125
    this.camera.position.y +=
      (-0.125 * this.mouseY - this.camera.position.y) * 0.0125

    this.camera.lookAt(this.scene.position)

    const delta = this.clock.getDelta()

    for (let i = 0; i < N_OBJECTS; i++) {
      const mesh = this.meshes[i]

      mesh.rotation.x += 0.3 * delta
      mesh.rotation.y += 0.5 * delta

      mesh.position.y = (mesh.position.y - 150 * delta) % 8000
    }

    this.renderer.render(this.scene, this.camera)
  }

  bindNode(node) {
    this.container = node
  }

  render() {
    // workaround for using window with gatsby build:
    // https://www.gatsbyjs.org/docs/debugging-html-builds/#how-to-check-if-window-is-defined
    if (typeof window === "undefined") {
      return null
    }
    const hasWebGl =
      !!window.WebGLRenderingContext &&
      !!document.createElement("canvas").getContext("experimental-webgl")
    if (!hasWebGl) {
      const message = window.WebGLRenderingContext
        ? "Your graphics card does not seem to support "
        : "Your browser does not seem to support "
      return (
        <div>
          {message}
          <a href="http://khronos.org/webgl/wiki/Getting_a_WebGL_Implementation">
            WebGL
          </a>.<br />
          Find out how to get it<a href="http://get.webgl.org/">here</a>.
        </div>
      )
    }

    return (
      <div
        className={css({
          background: "#050505",
          padding: 0,
          margin: 0,
          fontWeight: "bold",
          overflow: "hidden",
          height: "100%",
          width: "100%",
        })}
      >
        <div ref={this.bindNode.bind(this)} />
      </div>
    )
  }
}

export default IndexPage
