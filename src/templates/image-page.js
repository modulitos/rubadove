import * as PropTypes from "prop-types"
import React from "react"
import Img from "gatsby-image"
import { css } from "react-emotion"
import { navigateTo } from "gatsby-link"
import ArrowForward from "material-ui/svg-icons/navigation/arrow-forward"
import ArrowBack from "material-ui/svg-icons/navigation/arrow-back"
import IconButton from "material-ui/IconButton"
import debounce from "debounce"

const styles = {
  imageContainer: {
    display: "grid",
    height: "inherit",
    width: "100%",
    gridTemplateAreas: `"left-button pic right-button"`,
    gridTemplateColumns: "100px 1fr 100px",
  },
  leftButtonContainer: {
    gridArea: "left-button",
    maxWidth: "100px",
    display: "grid",
  },
  rightButtonContainer: {
    gridArea: "right-button",
    maxWidth: "100px",
    display: "grid",
  },
  navButton: {
    alignSelf: "center",
  },
  picContainer: {
    justifyContent: "center",
    gridArea: "pic",
    alignSelf: "center",
    height: "inherit",
  },
}

class SlideTemplate extends React.Component {
  constructor(props) {
    super(props)

    const { id } = props.data.slidesJson
    const slides = props.data.allSlidesJson.edges.map(e => e.node)
    const curr = slides.find(slide => slide.id === id)
    const currIndex = slides.indexOf(curr)

    const prevIndex = (currIndex - 1 + slides.length) % slides.length
    this.prevSlideId = slides[prevIndex].id
    const nextIndex = (currIndex + 1 + slides.length) % slides.length
    this.nextSlideId = slides[nextIndex].id

    this.onArrowPress = debounce(this.onArrowPress.bind(this), 1000, true)
  }

  componentDidMount() {
    document.addEventListener("keydown", this.onArrowPress, false)
  }
  componentWillUnmount() {
    document.removeEventListener("keydown", this.onArrowPress, false)
  }
  navigatePreviousSlide() {
    navigateTo(`/espana-2017/${this.prevSlideId}`)
  }
  navigateNextSlide() {
    navigateTo(`/espana-2017/${this.nextSlideId}`)
  }

  onArrowPress(event) {
    event.preventDefault()
    event.stopPropagation()
    const { keyCode } = event
    if (keyCode === 37 || keyCode === 40) {
      this.navigatePreviousSlide()
    } else if (keyCode === 38 || keyCode === 39) {
      this.navigateNextSlide()
    }
  }

  render() {
    const {
      bigImage,
      // sizes,
    } = this.props.data.slidesJson
    const { big } = bigImage.childImageSharp

    return (
      <div
        onClick={e => e.stopPropagation()}
        className={css(styles.imageContainer)}
      >
        <div className={css(styles.leftButtonContainer)}>
          <IconButton
            className={css(styles.navButton)}
            onClick={this.navigatePreviousSlide.bind(this)}
          >
            <ArrowBack />
          </IconButton>
        </div>
        <Img
          sizes={big}
          className={css({ height: "inherit" })}
          outerWrapperClassName={css(styles.picContainer)}
          imgStyle={{
            objectFit: "scale-down",
          }}
        />
        <div className={css(styles.rightButtonContainer)}>
          <IconButton
            className={css(styles.navButton)}
            onClick={this.navigateNextSlide.bind(this)}
          >
            <ArrowForward />
          </IconButton>
        </div>
      </div>
    )
  }
}

SlideTemplate.propTypes = {
  data: PropTypes.shape({
    slidesJson: PropTypes.object.isRequired,
    allSlidesJson: PropTypes.object.isRequired,
  }),
}

export default SlideTemplate

// The post template's GraphQL query. Notice the “id”
// variable which is passed in. We set this on the page
// context in gatsby-node.js.
//
// All GraphQL queries in Gatsby are run at build-time and
// loaded as plain JSON files so have minimal client cost.
export const pageQuery = graphql`
  query SlidePage($id: String!) {
    # Select the image which equals this id.
    slidesJson(id: { eq: $id }) {
      # Specify the fields from the post we need.
      id
      text

      bigImage: image {
        childImageSharp {
          # Here we query for *multiple* image thumbnails to be
          # created. So with no effort on our part, 100s of
          # thumbnails are created. This makes iterating on
          # designs effortless as we change the args
          # for the query and we get new thumbnails.
          big: sizes(maxWidth: 640) {
            ...GatsbyImageSharpSizes
          }
        }
      }
    }
    allSlidesJson {
      edges {
        node {
          id
          text
        }
      }
    }
  }
`
